import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Persaratan extends StatefulWidget {
  Persaratan({Key key}) : super(key: key);

  _PersaratanState createState() => _PersaratanState();
}

class _PersaratanState extends State<Persaratan> {

  Future<File> imageFileKtp;
  Future<File> imageFileKtpProfile;

  String type = "";
 
  pickImageFromGallery(ImageSource source) {
    setState(() {
      if(type=="ktp"){
        imageFileKtp = ImagePicker.pickImage(source: source);
      } else {
        imageFileKtpProfile = ImagePicker.pickImage(source: source);
      }
    });
  }

  Widget showImageKtp() {
    return FutureBuilder<File>(
      future: imageFileKtp,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: 200,
            height: 200,
            fit: BoxFit.fill,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return Image.asset("assets/images/anonymous.jpg", height: 200, fit: BoxFit.fill);
          // return const Text(
          //   'No Image Selected',
          //   textAlign: TextAlign.center,
          // );
        }
      },
    );
  }

  Widget showImageKtpProfile() {
    return FutureBuilder<File>(
      future: imageFileKtpProfile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: 200,
            height: 200,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return Image.asset("assets/images/anonymous.jpg", height: 200, fit: BoxFit.fill);
          // return const Text(
          //   'No Image Selected',
          //   textAlign: TextAlign.center,
          // );
        }
      },
    );
  }

  void _settingModalBottomSheet(context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
        return Container(
          child: new Wrap(
            children: <Widget>[
              new ListTile(
                leading: new Icon(Icons.image),
                title: new Text('Gallery'),
                onTap: () {
                  pickImageFromGallery(ImageSource.gallery);
                  Navigator.pop(context);
                }          
              ),
              new ListTile(
                leading: new Icon(Icons.camera_alt),
                title: new Text('Camera'),
                onTap: () {
                  pickImageFromGallery(ImageSource.camera);
                  Navigator.pop(context);
                },          
              ),
            ],
          ),
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Persaratan"),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blue, width: 1, style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(10),
                color: Colors.white
              ),
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 50),
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Column(
                children: <Widget>[
                  showImageKtp(),
                  SizedBox(
                    height: 10,
                  ),
                  FlatButton(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                        width: 1.0, 
                        color: Colors.blue
                      ), 
                      borderRadius: BorderRadius.circular(30)
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 45),
                      child: Text("Choose KTP")
                    ),
                    onPressed: (){
                      setState(() {
                        type = "ktp";
                      });
                      _settingModalBottomSheet(context);
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blue, width: 1, style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(10),
                color: Colors.white
              ),
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 50),
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              child: Column(
                children: <Widget>[
                  showImageKtpProfile(),
                  SizedBox(
                    height: 10,
                  ),
                  FlatButton(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                        width: 1.0,
                        color: Colors.blue
                      ), 
                      borderRadius: BorderRadius.circular(30)
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text("Choose Profile KTP")
                    ),
                    onPressed: (){
                      setState(() {
                        type = "ktp_profile";
                      });
                      _settingModalBottomSheet(context);
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 50),
              child: FlatButton(
                color: Colors.white,
                shape: RoundedRectangleBorder(side: const BorderSide(width: 1.0, color: Colors.blue), borderRadius: BorderRadius.circular(30)),
                child: Text(" Save ",style: TextStyle(wordSpacing: 50, color: Colors.blue),),
                onPressed: () {},
              ),
            )
          ],
        ),
      ),
    );
  }
}