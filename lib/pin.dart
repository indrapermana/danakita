import 'dart:async';

import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/pin_widget.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';

class PinPage extends StatefulWidget {
  PinPage({Key key}) : super(key: key);

  _PinPageState createState() => _PinPageState();
}

class _PinPageState extends State<PinPage> {

  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  bool isAuthenticated = false;

  CircleUIConfig circleUIConfig = CircleUIConfig(borderColor: Colors.blue[600], fillColor: Colors.white, circleSize: 25, borderWidth: 2);
  KeyboardUIConfig keyboardUIConfig = KeyboardUIConfig(digitBorderWidth: 2, primaryColor: Colors.blue[400]);

  @override
  Widget build(BuildContext context) {
    return PinWidget(
      title: 'Masukan PIN',
      circleUIConfig: circleUIConfig,
      keyboardUIConfig: keyboardUIConfig,
      passwordEnteredCallback: _onPasscodeEntered,
      cancelLocalizedText: 'Remove',
      deleteLocalizedText: 'Delete',
      shouldTriggerVerification: _verificationNotifier.stream,
      backgroundColor: Colors.black.withOpacity(0.8),
      cancelCallback: _onPasscodeCancelled,
    );
  }

  _onPasscodeEntered(String enteredPasscode) {
    bool isValid = '123456' == enteredPasscode;
    _verificationNotifier.add(isValid);
    if (isValid) {
      setState(() {
        this.isAuthenticated = isValid;
      });
    }
  }

  _onPasscodeCancelled() {

  }

  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }
}