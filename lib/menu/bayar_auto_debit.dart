import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/menu/bayar_transaksi.dart';

class BayarAutoDebit extends StatefulWidget {
  BayarAutoDebit({Key key}) : super(key: key);

  _BayarAutoDebitState createState() => _BayarAutoDebitState();
}

class _BayarAutoDebitState extends State<BayarAutoDebit> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(15),
          child: InkWell(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/images/payment.png", width: 40,),
                SizedBox(width: 10,),
                Expanded(
                  child: Text("Pendaftaran Auto Debit", style: TextStyle(color: Colors.black54),),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.black38, size: 16,)
              ],
            ),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => BayarTransaksi(title: "Pendaftaran Auto Debit",)
              ));
            },
          ),
        ),
        Container(
          padding: EdgeInsets.all(15),
          child: InkWell(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/images/payment.png", width: 40,),
                SizedBox(width: 10,),
                Expanded(
                  child: Text("Pebatalan Auto Debit", style: TextStyle(color: Colors.black54),),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.black38, size: 16,)
              ],
            ),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => BayarTransaksi(title: "Pembatalan Auto Debit",)
              ));
            },
          ),
        ),
      ],
    );
  }
}