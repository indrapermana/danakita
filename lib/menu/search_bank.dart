import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/menu/transfer_bank.dart';

class SearchBank extends StatefulWidget {
  SearchBank({Key key}) : super(key: key);

  _SearchBankState createState() => _SearchBankState();
}

class _SearchBankState extends State<SearchBank> {

  TextEditingController searchController = TextEditingController();

  List<Map<String, String>> _bank = [
    {
      "kode_bank" : "1",
      "nama_bank" : "BNI",
    },
    {
      "kode_bank" : "2",
      "nama_bank" : "BRI",
    },
    {
      "kode_bank" : "3",
      "nama_bank" : "BTN",
    },
    {
      "kode_bank" : "4",
      "nama_bank" : "BCA",
    },
    {
      "kode_bank" : "5",
      "nama_bank" : "Mandiri",
    },
  ];
  var dataBank = List<Map<String, String>>();

  // final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  // var items = List<String>();

  @override
  void initState() {
    // items.addAll(duplicateItems);
    dataBank.addAll(_bank);
    super.initState();
  }

  void filterSearchResults(String search) {
    List<Map<String, String>> dummySearchList = List<Map<String, String>>();
    dummySearchList.addAll(_bank);
    if(search.isNotEmpty || search!=""){
      List<Map<String, String>> dummyListData = List<Map<String, String>>();
      for (var bank in dummySearchList) {
        if(bank['nama_bank'].contains(search)){
          dummyListData.add(bank);
        }
      }
      setState(() {
        dataBank.clear();
        dataBank.addAll(dummyListData);
      });
      print("search data bank : ${dataBank.toString()}");
    } else {
      setState(() {
        dataBank.clear();
        dataBank.addAll(_bank);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          controller: searchController,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          textCapitalization: TextCapitalization.characters,
          decoration: InputDecoration(
            hintText: "Cari",
            hintStyle: TextStyle(fontWeight: FontWeight.w300, color: Colors.white)
          ),
          onChanged: (value){
            filterSearchResults(value);
          },
        ),
      ),
      body: ListView.builder(
        padding: EdgeInsets.only(top: 10),
        shrinkWrap: true,
        itemCount: dataBank.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("${dataBank[index]['nama_bank']}",),
                SizedBox(height: 5,),
                Text(dataBank[index]['kode_bank'], style: TextStyle(color: Colors.black45, fontSize: 12),),
                SizedBox(height: 10,),
                Divider(color: Colors.black45,)
              ],
            ),
            onTap: (){
              Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context) => TransferBank(
                    kodeBank: dataBank[index]['kode_bank'], 
                    namaBank: dataBank[index]['nama_bank'],
                  )
              ));
            },
          );
        },
      ),
    );
  }
}