import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/menu/search_instansi.dart';

class BayarTransaksi extends StatefulWidget {
  final String title;
  final String labels;
  final String kodeInstansi;
  final String namaInstansi;
  BayarTransaksi({Key key, @required this.title, this.labels, this.kodeInstansi, this.namaInstansi}) : super(key: key);

  _BayarTransaksiState createState() => _BayarTransaksiState();
}

class _BayarTransaksiState extends State<BayarTransaksi> {

  TextEditingController jasaCtr = new TextEditingController();
  TextEditingController nomorCtr = new TextEditingController();
  TextEditingController pesanCtr = new TextEditingController();

  @override
  void initState() {
    if(widget.kodeInstansi!=""){
      jasaCtr.text = widget.namaInstansi;
    }
    super.initState();
  }

  Widget titles(){
    if(widget.title==null){
      return Text(" ");
    }
    return Text(widget.title);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(20),
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset("assets/images/banner_danakita.jpeg", width: 80,),
                    ),
                    SizedBox(width: 10,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Muhammad Indra Permana", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w900),),
                        SizedBox(height: 5,),
                        Text("123456789098")
                      ],
                    )
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("SALDO"),
                    Text("Rp. 8.0000.000", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),)
                  ],
                )
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 10,
            color: Colors.black12,
          ),
          Container(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextField(
                  controller: jasaCtr,
                  decoration: new InputDecoration(
                    hintText: "Penyedia Jasa", 
                    labelText: "Pilih Instansi",
                  ),
                  keyboardType: TextInputType.number,
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) => SearchInstansi(title: widget.title,)
                    ));
                  },
                ),
                (widget.labels!=null)? TextField(
                  controller: nomorCtr,
                  decoration: new InputDecoration(
                    hintText: widget.labels, 
                    labelText: "Masukan Nomor Pelanggan",
                  ),
                ) : Container(),
                TextField(
                  controller: pesanCtr,
                  decoration: new InputDecoration(
                    hintText: "Deskripsi (optional)", 
                    labelText: "Masukkan Deskripsi",
                  ),
                  maxLength: 200,
                ),
              ],
            ),
          )
        ],
      ),
      bottomSheet: InkWell(
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: Colors.blue,
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Text("LANJUNTKAN", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16), textAlign: TextAlign.center,),
        ),
      ),
    );
  }
}