import 'package:flutter/material.dart';
import 'package:indonesia/indonesia.dart';

class VerifikasiTarikTunai extends StatefulWidget {
  final int nominal;
  VerifikasiTarikTunai(this.nominal, {Key key}) : super(key: key);

  _VerifikasiTarikTunaiState createState() => _VerifikasiTarikTunaiState();
}

class _VerifikasiTarikTunaiState extends State<VerifikasiTarikTunai> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("VERIFIKASI"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: (){},
          )
        ],
        elevation: 0.0,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.all(20),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black45, width: 1, style: BorderStyle.solid),
                borderRadius: BorderRadius.circular(10)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Nominal Tarik Anda", style: TextStyle(fontWeight: FontWeight.w500),),
                  Divider(color: Colors.black45, thickness: 2,),
                  Text(rupiah(widget.nominal), style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),)
                ],
              ),
            ),
            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: RaisedButton(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text("LANJUTKAN", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18)),
                ),
                color: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                  side: BorderSide(color: Colors.blue, width: 1, style: BorderStyle.solid)
                ),
                elevation: 0.0,
                padding: EdgeInsets.symmetric(horizontal: 108),
                onPressed: () {
                  // Navigator.push(context, MaterialPageRoute(
                  //   builder: (context) => WalletTransValidation(id: widget.id,)
                  // ));
                }
              ),
            )
          ],
        ),
      ),
    );
  }
}