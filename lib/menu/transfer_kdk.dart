import 'package:flutter/material.dart';

class TransferKDK extends StatefulWidget {
  TransferKDK({Key key}) : super(key: key);

  _TransferKDKState createState() => _TransferKDKState();
}

class _TransferKDKState extends State<TransferKDK> {

  TextEditingController nomorCtr = new TextEditingController();
  TextEditingController nominalCtr = new TextEditingController();
  TextEditingController pesanCtr = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KE SESAMA ANGGOTA"),
      ),
      body: ListView(
        padding: EdgeInsets.all(30),
        children: <Widget>[
          TextField(
            controller: nomorCtr,
            decoration: new InputDecoration(
              hintText: "Masukkan Nomor KDK", 
              labelText: "Masukkan Nomor KDK",
              icon: Icon(Icons.contacts)
            ),
            keyboardType: TextInputType.number,
          ),
          SizedBox(height: 20),
          Text("Sumber Dana"),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black26, width: 1.5, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.asset("assets/images/danakita.png", width: 40,),
                ),
                SizedBox(width: 7,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("KDK CASH", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("Saldo"),
                        SizedBox(width: 5,),
                        Text("Rp 100.000", style: TextStyle(fontWeight: FontWeight.bold),)
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 20,),
          Container(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 30),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(10)
            ),
            child: TextField(
              controller: nominalCtr,
              style: TextStyle(fontSize: 20, color: Colors.black),
              maxLength: 8,
              decoration: new InputDecoration(
                labelText: "Nominal Transfer",
                focusColor: Colors.grey[200],
                disabledBorder: InputBorder.none,
                labelStyle: TextStyle(fontSize: 16)
              ),
              keyboardType: TextInputType.number,
            ),
          ),
          SizedBox(height: 20,),
          Container(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 30),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(10)
            ),
            child: TextField(
              controller: pesanCtr,
              maxLength: 24,
              decoration: new InputDecoration(
                labelText: "Pesan (optional)",
                focusColor: Colors.grey[200],
              ),
              keyboardType: TextInputType.number,
            ),
          ),
          SizedBox(height: 20,),
          RaisedButton(
            child: new Text("LANJUTKAN", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18)),
            color: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)
            ),
            elevation: 0.0,
            padding: EdgeInsets.symmetric(horizontal: 70),
            onPressed: () {}
          )
        ],
      ),
    );
  }
}