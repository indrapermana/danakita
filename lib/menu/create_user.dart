import 'package:flutter/material.dart';

class CreateUserPage extends StatefulWidget {
  CreateUserPage({Key key}) : super(key: key);

  _CreateUserPageState createState() => _CreateUserPageState();
}

class _CreateUserPageState extends State<CreateUserPage> {

  TextEditingController nikCtr = new TextEditingController();
  TextEditingController namaCtr = new TextEditingController();
  TextEditingController jenisKelaminCtr = new TextEditingController();
  TextEditingController alamatCtr = new TextEditingController();
  TextEditingController jabatanCtr = new TextEditingController();
  TextEditingController divisiCtr = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Buat Akun"),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          TextField(
            controller: nikCtr,
            decoration: new InputDecoration(
              hintText: "Masukkan NIK", 
              labelText: "Masukkan NIK",
            ),
          ),
          TextField(
            controller: namaCtr,
            decoration: new InputDecoration(
              hintText: "Masukkan Nama Karyawan", 
              labelText: "Nama Karyawan",
            ),
          ),
          TextField(
            controller: jenisKelaminCtr,
            decoration: new InputDecoration(
              hintText: "Masukkan Jenis Kelamin", 
              labelText: "Jenis Kelamin",
            ),
          ),
          TextField(
            controller: alamatCtr,
            maxLines: 3,
            decoration: new InputDecoration(
              hintText: "Masukkan Alamat", 
              labelText: "Alamat Karyawan",
            ),
          ),
          TextField(
            controller: jabatanCtr,
            decoration: new InputDecoration(
              hintText: "Masukkan Jabatan", 
              labelText: "Jabatan",
            ),
          ),
          TextField(
            controller: divisiCtr,
            decoration: new InputDecoration(
              hintText: "Masukkan Divisi", 
              labelText: "Divisi",
            ),
          ),
          SizedBox(height: 20),
          RaisedButton(
            child: new Text("SIMPAN", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18)),
            color: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)
            ),
            elevation: 0.0,
            padding: EdgeInsets.symmetric(horizontal: 70),
            onPressed: () {}
          )
        ],
      ),
    );
  }
}