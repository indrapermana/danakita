import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/menu/bayar_auto_debit.dart';
import 'package:koperasi_dana_kita/menu/bayar_one_time.dart';

class Bayar extends StatefulWidget {
  Bayar({Key key}) : super(key: key);

  _BayarState createState() => _BayarState();
}

class _BayarState extends State<Bayar> with SingleTickerProviderStateMixin {

  TabController controller;

  @override
  void initState() { 
    controller = TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() { 
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pembayaran"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.list),
            onPressed: (){},
          )
        ],
        bottom: TabBar(
          controller: controller,
          tabs: <Widget>[
            Tab(text: "ONE TIME",),
            Tab(text: "AUTO DEBIT",)
          ],
        ),
      ),
      body: TabBarView(
        controller: controller,
        children: <Widget>[
          BayarOneTime(),
          BayarAutoDebit()
        ],
      ),
    );
  }
}