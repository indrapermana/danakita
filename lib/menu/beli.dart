import 'package:flutter/material.dart';

class Beli extends StatefulWidget {
  Beli({Key key}) : super(key: key);

  _BeliState createState() => _BeliState();
}

class _BeliState extends State<Beli> {

  bool showDetail = false;
  IconData iconArrow = Icons.keyboard_arrow_down;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pembelian"),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          InkWell(
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/images/payment.png", width: 50,),
                SizedBox(width: 10,),
                Expanded(
                  child: Text("Buat Pembelian Baru"),
                ),
                Icon(iconArrow)
              ],
            ),
          ),
          onTap: (){
            if(showDetail){
              setState(() {
                showDetail = false;
                iconArrow = Icons.keyboard_arrow_down;
              });
            } else {
              setState(() {
                showDetail = true;
                iconArrow = Icons.keyboard_arrow_up;
              });
            }
          },
        ),
        pembelian(),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 10,
          color: Colors.grey[100],
        ),
        Container(
          padding: EdgeInsets.only(left: 15, top: 15),
          color: Colors.white,
          child: Text("PEMBELIAN TERBARU", style: TextStyle(color: Colors.black54),)
        ),
        historyPembayaran()
        ],
      ),
    );
  }

  Widget pembelian(){
    if (!showDetail) {
      return Container();
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 15),
      color: Colors.blue[50],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          btnIcon(Icons.phone_iphone, Colors.blue, "Pulsa", context),
          btnIcon(Icons.speaker_phone, Colors.blue, "Paket Data", context),
          btnIcon(Icons.wb_incandescent, Colors.blue, "PLN Prabayar", context),
        ],
      ),
    );
  }

  Widget btnIcon(IconData icon, Color bgColor, String label, BuildContext context) {
    return InkWell(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: bgColor,
              borderRadius: BorderRadius.circular(50)
            ),
            child: Icon(icon, size: 35, color: Colors.white,),
          ),
          SizedBox(height: 5,),
          Text(label, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w400, fontSize: 12),)
        ],
      ),
      onTap: () {
        // Navigator.push(context, MaterialPageRoute(
        //   builder: (context) => BayarTransaksi(title: label,)
        // ));
      },
    );
  }

  Widget historyPembayaran(){
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset("assets/images/payment.png", width: 50,),
                  SizedBox(width: 10,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("tokpen", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),),
                        SizedBox(height: 5,),
                        Text("78687558768", style: TextStyle(color: Colors.black45, fontSize: 12),)
                      ],
                    ),
                  ),
                  Icon(Icons.arrow_forward_ios, color: Colors.black54, size: 20,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}