import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/menu/transfer_bank.dart';
import 'package:koperasi_dana_kita/menu/transfer_kdk.dart';

class Transfer extends StatefulWidget {
  Transfer({Key key}) : super(key: key);

  _TransferState createState() => _TransferState();
}

class _TransferState extends State<Transfer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Transfer"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.portrait),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.calendar_today),
            onPressed: (){},
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(bottom: 20),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.black12, 
                  width: 10, 
                  style: BorderStyle.solid
                )
              )
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text("Transfer data", style: TextStyle(fontSize: 16),),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.black12, 
                        width: 1, 
                        style: BorderStyle.solid
                      ), 
                      top: BorderSide(
                        color: Colors.black12,
                        width: 1, 
                        style: BorderStyle.solid
                      )
                    )
                  ),
                  child: InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Icon(Icons.account_circle, color: Colors.blue[300],),
                        ),
                        Expanded(
                          child: Text("Transfer ke Danakita"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.black38,),
                        )
                      ],
                    ),
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => TransferKDK()
                      ));
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.black12, 
                        width: 1,
                        style: BorderStyle.solid
                      ), 
                    )
                  ),
                  child: InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Icon(Icons.account_balance, color: Colors.blue[300],),
                        ),
                        Expanded(
                          child: Text("Transfer ke bank"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Icon(Icons.arrow_forward_ios, size: 18, color: Colors.black38,),
                        )
                      ],
                    ),
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => TransferBank()
                      ));
                    },
                  ),
                )
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.all(10),
            child: Text("History Transfer", style: TextStyle(fontSize: 16),),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _historyTransfer("ARIE LESMANA HIDAYAT", "KDk", "89776587665", "kdk"),
                _historyTransfer("ARIE LESMANA HIDAYAT", "Mandiri", "09876543212", "bank"),
                _historyTransfer("ARIE LESMANA HIDAYAT", "KDK", "89776587665", "kdk"),
                _historyTransfer("ARIE LESMANA HIDAYAT", "Mandiri", "09876543212", "bank"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _historyTransfer(String nama, String bank, String norek, String type,){
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: Colors.black12, width: 1, style: BorderStyle.solid))
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.black12
            ),
            padding: EdgeInsets.all(3),
            child: Center(child: Icon((type=="kdk")? Icons.account_circle : Icons.account_balance, size: 30, color: Colors.white)),
          ),
          SizedBox(width: 10,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(nama, style: TextStyle(fontSize: 16),),
              Text("$bank - $norek", style: TextStyle(fontSize: 12),)
            ],
          ),
        ],
      ),
    );
  }
}