import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/menu/bayar_transaksi.dart';

class BayarOneTime extends StatefulWidget {
  BayarOneTime({Key key}) : super(key: key);

  _BayarOneTimeState createState() => _BayarOneTimeState();
}

class _BayarOneTimeState extends State<BayarOneTime> {

  bool showDetail = false;
  IconData iconArrow = Icons.keyboard_arrow_down;

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        InkWell(
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/images/payment.png", width: 50,),
                SizedBox(width: 10,),
                Expanded(
                  child: Text("Buat Pembayaran Baru"),
                ),
                Icon(iconArrow)
              ],
            ),
          ),
          onTap: (){
            if(showDetail){
              setState(() {
                showDetail = false;
                iconArrow = Icons.keyboard_arrow_down;
              });
            } else {
              setState(() {
                showDetail = true;
                iconArrow = Icons.keyboard_arrow_up;
              });
            }
          },
        ),
        pembayaran(),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 10,
          color: Colors.black12,
        ),
        historyPembayaran()
      ],
    );
  }

  Widget historyPembayaran(){
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("PEMBAYARAN TERBARU", style: TextStyle(color: Colors.black54),),
          SizedBox(height: 15,),
          InkWell(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset("assets/images/payment.png", width: 50,),
                  SizedBox(width: 10,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("tokpen", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),),
                        SizedBox(height: 5,),
                        Text("78687558768", style: TextStyle(color: Colors.black45, fontSize: 12),)
                      ],
                    ),
                  ),
                  Icon(Icons.arrow_forward_ios, color: Colors.black54, size: 20,)
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/images/payment.png", width: 50,),
                SizedBox(width: 10,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("tokpen", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),),
                      SizedBox(height: 5,),
                      Text("78687558768", style: TextStyle(color: Colors.black45, fontSize: 12),)
                    ],
                  ),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.black54, size: 20,)
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/images/payment.png", width: 50,),
                SizedBox(width: 10,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("tokpen", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w500),),
                      SizedBox(height: 5,),
                      Text("78687558768", style: TextStyle(color: Colors.black45, fontSize: 12),)
                    ],
                  ),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.black54, size: 20,)
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/images/payment.png", width: 50,),
                SizedBox(width: 10,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("tokpen", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w500),),
                      SizedBox(height: 5,),
                      Text("78687558768", style: TextStyle(color: Colors.black45, fontSize: 12),)
                    ],
                  ),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.black54, size: 20,)
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("assets/images/payment.png", width: 50,),
                SizedBox(width: 10,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("tokpen", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w500),),
                      SizedBox(height: 5,),
                      Text("78687558768", style: TextStyle(color: Colors.black45, fontSize: 12),)
                    ],
                  ),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.black54, size: 20,)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget pembayaran(){
    if(showDetail){
      return Container(
        padding: EdgeInsets.symmetric(vertical: 15),
        color: Colors.blue[50],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                btnIcon(Icons.wifi, Colors.blue, "Internet", context),
                btnIcon(Icons.call, Colors.blue, "Telekomunikasi", context),
                btnIcon(Icons.school, Colors.blue, "Pendidikan", context),
              ],
            ),
            SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: btnIcon(Icons.live_tv, Colors.blue, "Kabel TV", context),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 39, right: 45),
                  child: btnIcon(Icons.description, Colors.blue, "Penerimaan Negara", context),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 0),
                  child: btnIcon(Icons.event, Colors.blue, "Angsuran", context),
                ),
              ],
            ),
            SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: btnIcon(Icons.opacity, Colors.blue, "PAM", context),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 66, right: 60),
                  child: btnIcon(Icons.beach_access, Colors.blue, "Asuransi", context),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 0),
                  child: btnIcon(Icons.payment, Colors.blue, "Multipayment", context),
                ),
              ],
            ),
            SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: btnIcon(Icons.receipt, Colors.blue, "Tiket", context),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 28),
                  child: btnIcon(Icons.payment, Colors.blue, "Kartu Kredit", context),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 13),
                  child: btnIcon(Icons.wb_incandescent, Colors.blue, "PLN", context),
                ),
              ],
            ),
            SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 33),
                  child: btnIcon(Icons.more_horiz, Colors.blue, "Lainnya", context),
                ),
              ],
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget btnIcon(gambar, bgColor, label, BuildContext context) {
    return InkWell(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: bgColor,
              borderRadius: BorderRadius.circular(50)
            ),
            child: Icon(gambar, size: 35, color: Colors.white,),
          ),
          SizedBox(height: 5,),
          Text(label, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w400, fontSize: 12),)
        ],
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => BayarTransaksi(title: label,)
        ));
      },
    );
  }
}