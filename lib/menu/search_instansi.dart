import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/menu/bayar_transaksi.dart';

class SearchInstansi extends StatefulWidget {
  final String title;
  SearchInstansi({Key key, @required this.title}) : super(key: key);

  _SearchInstansiState createState() => _SearchInstansiState();
}

class _SearchInstansiState extends State<SearchInstansi> {
  TextEditingController searchController = TextEditingController();

  List<Map<String, String>> _bank = [
    {
      "kode_bank" : "1",
      "nama_bank" : "BOLT isi Ulang",
      "label" : "Nomor BOLT",
    },
    {
      "kode_bank" : "2",
      "nama_bank" : "BOLT Pembayaran",
      "label" : "Nomor BOLT",
    },
    {
      "kode_bank" : "3",
      "nama_bank" : "Bina Informatika, PT",
      "label" : "Virtual Account",
    },
    {
      "kode_bank" : "4",
      "nama_bank" : "CBN",
      "label" : "Virtual Account",
    },
    {
      "kode_bank" : "5",
      "nama_bank" : "Citranet",
      "label" : "Virtual Account",
    },
  ];
  var dataBank = List<Map<String, String>>();

  // final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  // var items = List<String>();

  @override
  void initState() {
    // items.addAll(duplicateItems);
    dataBank.addAll(_bank);
    super.initState();
  }

  void filterSearchResults(String search) {
    List<Map<String, String>> dummySearchList = List<Map<String, String>>();
    dummySearchList.addAll(_bank);
    if(search.isNotEmpty || search!=""){
      List<Map<String, String>> dummyListData = List<Map<String, String>>();
      for (var bank in dummySearchList) {
        if(bank['nama_bank'].contains(search)){
          dummyListData.add(bank);
        }
      }
      setState(() {
        dataBank.clear();
        dataBank.addAll(dummyListData);
      });
      print("search data instansi : ${dataBank.toString()}");
    } else {
      setState(() {
        dataBank.clear();
        dataBank.addAll(_bank);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print(widget.title);
    // print("data Bank : ${dataBank.toString()}");
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          controller: searchController,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          textCapitalization: TextCapitalization.characters,
          decoration: InputDecoration(
            hintText: "Cari",
            hintStyle: TextStyle(fontWeight: FontWeight.w300, color: Colors.white)
          ),
          onChanged: (value){
            filterSearchResults(value);
          },
        ),
      ),
      body: ListView.builder(
        padding: EdgeInsets.only(top: 10),
        shrinkWrap: true,
        itemCount: dataBank.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("${dataBank[index]['nama_bank']}",),
                SizedBox(height: 5,),
                Text(dataBank[index]['kode_bank'], style: TextStyle(color: Colors.black45, fontSize: 12),),
                SizedBox(height: 10,),
                Divider(color: Colors.black45,)
              ],
            ),
            onTap: (){
              print(widget.title);
              Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context) => BayarTransaksi(
                    title: widget.title,
                    kodeInstansi: dataBank[index]['kode_bank'], 
                    namaInstansi: dataBank[index]['nama_bank'],
                    labels: dataBank[index]['label'],
                  )
              ));
            },
          );
        },
      ),
    );
  }
}