import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/menu/verifikasi_tarik_tunai.dart';

class TarikTunai extends StatefulWidget {
  TarikTunai({Key key}) : super(key: key);

  _TarikTunaiState createState() => _TarikTunaiState();
}

class _TarikTunaiState extends State<TarikTunai> {
  TextEditingController nomorCtr = new TextEditingController();
  TextEditingController nominalCtr = new TextEditingController();
  TextEditingController pesanCtr = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tarik Tunai"),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          TextField(
            controller: nomorCtr,
            decoration: new InputDecoration(
              hintText: "Nomor Rekening", 
              labelText: "Masukkan Nomor Rekening",
            ),
            keyboardType: TextInputType.number,
          ),
          SizedBox(height: 20),
          Text("Sumber Dana"),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black26, width: 1.5, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.asset("assets/images/danakita.png", width: 40,),
                ),
                SizedBox(width: 7,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("TDA CASH", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("Saldo"),
                        SizedBox(width: 5,),
                        Text("Rp 100.000", style: TextStyle(fontWeight: FontWeight.bold),)
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 20,),
          Container(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 30),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(10)
            ),
            child: TextField(
              controller: nominalCtr,
              style: TextStyle(fontSize: 20, color: Colors.black),
              maxLength: 8,
              decoration: new InputDecoration(
                labelText: "Nominal Tarik",
                focusColor: Colors.grey[200],
                disabledBorder: InputBorder.none,
                labelStyle: TextStyle(fontSize: 16),
                icon: Text("Rp. ", style: TextStyle(fontSize: 16))
              ),
              keyboardType: TextInputType.number,
            ),
          ),
          SizedBox(height: 20,),
          Container(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10)
            ),
            child: TextField(
              controller: pesanCtr,
              maxLength: 200,
              decoration: new InputDecoration(
                labelText: "Pesan (Optional)",
              ),
            ),
          ),
          SizedBox(height: 20,),
          RaisedButton(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Text("LANJUTKAN", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 18)),
            ),
            color: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)
            ),
            elevation: 0.0,
            padding: EdgeInsets.symmetric(horizontal: 70),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => VerifikasiTarikTunai(int.parse(nominalCtr.text))
              ));
            }
          )
        ],
      ),
    );
  }
}