import 'package:flutter/material.dart';
import 'package:indonesia/indonesia.dart';

class TransaksiTertunda extends StatefulWidget {
  TransaksiTertunda({Key key}) : super(key: key);

  _TransaksiTertundaState createState() => _TransaksiTertundaState();
}

class _TransaksiTertundaState extends State<TransaksiTertunda> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Transaksi Tertunda"),
      ),
      body: ListView(
        children: <Widget>[
          header("12 Sep 2019"),
          listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098", 100000),
          listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098", 100000),
          listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098", 100000),
          header("13 Sep 2019"),
          listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098", 100000),
          listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098", 100000),
          listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098", 100000),
        ],
      ),
    );
  }

  Widget header(String tanggal){
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.grey[200],
      child: Text(tanggal),
    );
  }

  Widget listData(String nama, String bank, String norek, int nominal){
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black12, width: 1, style: BorderStyle.solid))
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(nama, style: TextStyle(fontWeight: FontWeight.w500),),
                  SizedBox(height: 5,),
                  Text("$bank - $norek", style: TextStyle(color: Colors.black45),),
                  SizedBox(height: 5,),
                  Text(rupiah(nominal)),
                ],
              ),
            ),
            Icon(Icons.arrow_forward_ios, size: 16, color: Colors.black38,)
          ],
        ),
      ),
      onTap: (){},
    );
  }
}