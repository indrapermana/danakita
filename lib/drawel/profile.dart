import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/persaratan/persaratan.dart';

class Profile extends StatelessWidget {
  const Profile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile Saya"),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue[400], Colors.blue[600]])
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white, width: 2, style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: CircleAvatar(
                      backgroundImage: AssetImage("assets/images/profile.jpg"),
                      radius: 40,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "MUHAMMAD INDRA PERMANA", 
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                    )
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            )
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("DETAIL AKUN", style: TextStyle(color: Colors.black38),),
                SizedBox(
                  height: 20,
                ),
                Text("Email", style: TextStyle(color: Colors.black38, fontSize: 12),),
                SizedBox(
                  height: 10,
                ),
                Text("indra.starts.permana@gmail.com"),
                SizedBox(
                  height: 20,
                ),
                Text("User ID", style: TextStyle(color: Colors.black38, fontSize: 12),),
                SizedBox(
                  height: 10,
                ),
                Text("indragotik"),
                SizedBox(
                  height: 20,
                ),
                Text("Nama Lengkap", style: TextStyle(color: Colors.black38, fontSize: 12),),
                SizedBox(
                  height: 10,
                ),
                Text("Muhammad Indra Permana"),
                SizedBox(
                  height: 20,
                ),
                Text("Bahasa", style: TextStyle(color: Colors.black38, fontSize: 12),),
                SizedBox(
                  height: 10,
                ),
                Text("Indonesia"),
                SizedBox(
                  height: 10,
                ),
                Text("Persaratan", style: TextStyle(color: Colors.black38, fontSize: 12),),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Text("Tidak Ada, harap di lengkapi dahulu "),
                    InkWell(
                      child: Text("Disini", style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline, fontWeight: FontWeight.bold),),
                      onTap: (){
                        Navigator.push(
                          context, 
                          MaterialPageRoute(
                            builder: (BuildContext context){
                              return Persaratan();
                            }
                          )
                        );
                      },
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: FlatButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(side: const BorderSide(width: 1.0, color: Colors.blue), borderRadius: BorderRadius.circular(30)),
              child: Text(" UBAH ",style: TextStyle(color: Colors.blue),),
              onPressed: () {},
            ),
          )
        ],
      ),
    );
  }
}