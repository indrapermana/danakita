import 'dart:async';

import 'package:flutter/material.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';
// import 'package:pin_code_text_field/pin_code_text_field.dart';
// import 'package:pin_view/pin_view.dart';

class PinPage extends StatefulWidget {
  PinPage({Key key}) : super(key: key);

  _PinPageState createState() => _PinPageState();
}

class _PinPageState extends State<PinPage> {

  // SmsListener smsListener = SmsListener(
  //   from: "085323778786",
  //   formatBody: (String body){
  //     // incoming message type
  //     // from: "6505551212"
  //     // body: "Your verification code is: 123-456"
  //     // with this function, we format body to only contain
  //     // the pin itself

  //     String codeRaw = body.split(": ")[1];
  //     List<String> code = codeRaw.split("-");
  //     return code.join();
  //   }
  // );

  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  bool isAuthenticated = false;

  CircleUIConfig circleUIConfig = CircleUIConfig(borderColor: Colors.blue, fillColor: Colors.blue, circleSize: 30);
  KeyboardUIConfig keyboardUIConfig = KeyboardUIConfig(digitBorderWidth: 2, primaryColor: Colors.blue);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blue[400],
        child: Column(
          children: <Widget>[
            SizedBox(height: 40,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset("assets/images/danakita.png", width: 20,),
                SizedBox(width: 10,),
                Text("Dana Kita", style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),)
              ],
            ),
            PasscodeScreen(
                title: 'Enter App Passcode',
                circleUIConfig: circleUIConfig,
                keyboardUIConfig: keyboardUIConfig,
                passwordEnteredCallback: _onPasscodeEntered,
                cancelLocalizedText: 'Cancel',
                deleteLocalizedText: 'Delete',
                shouldTriggerVerification: _verificationNotifier.stream,
                backgroundColor: Colors.black.withOpacity(0.8),
                cancelCallback: _onPasscodeCancelled,
              ),
            // _defaultLockScreenButton(context),
            // _customColorsLockScreenButton(context)
          ],
        ),
      ),
    );
  }

  Widget _defaultLockScreenButton(BuildContext context) => MaterialButton(
        color: Theme.of(context).primaryColor,
        child: Text('Open Default Lock Screen'),
        onPressed: () {
          _showLockScreen(context, opaque: false);
        },
      );

  Widget _customColorsLockScreenButton(BuildContext context) => MaterialButton(
        color: Theme.of(context).primaryColor,
        child: Text('Open Custom Lock Screen'),
        onPressed: () {
          _showLockScreen(context,
              opaque: false,
              circleUIConfig: CircleUIConfig(
                  borderColor: Colors.blue,
                  fillColor: Colors.blue,
                  circleSize: 30),
              keyboardUIConfig: KeyboardUIConfig(digitBorderWidth: 2, primaryColor: Colors.blue));
        },
      );

  _showLockScreen(BuildContext context,
      {bool opaque,
      CircleUIConfig circleUIConfig,
      KeyboardUIConfig keyboardUIConfig}) {
    Navigator.push(
        context,
        PageRouteBuilder(
          opaque: opaque,
          pageBuilder: (context, animation, secondaryAnimation) =>
              PasscodeScreen(
                title: 'Enter App Passcode',
                circleUIConfig: circleUIConfig,
                keyboardUIConfig: keyboardUIConfig,
                passwordEnteredCallback: _onPasscodeEntered,
                cancelLocalizedText: 'Cancel',
                deleteLocalizedText: 'Delete',
                shouldTriggerVerification: _verificationNotifier.stream,
                backgroundColor: Colors.black.withOpacity(0.8),
                cancelCallback: _onPasscodeCancelled,
              ),
        ));
  }

  _onPasscodeEntered(String enteredPasscode) {
    bool isValid = '123456' == enteredPasscode;
    _verificationNotifier.add(isValid);
    if (isValid) {
      setState(() {
        this.isAuthenticated = isValid;
      });
    }
  }

  _onPasscodeCancelled() {

  }

  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }

  // Widget pinViewWithSms(BuildContext context){
  //   return PinView(
  //     count: 6,
  //     dashPositions: [3],
  //     autoFocusFirstField: false,
  //     enabled: true,
  //     sms: smsListener,
  //     submit: (String pin){
  //       showDialog(
  //         context: context,
  //         builder: (context){
  //           return AlertDialog(
  //             title: Text("Pin received successfully."),
  //             content: Text("Entered pin is: $pin"),
  //           );
  //         }
  //       );
  //     },
  //   );
  // }
}
