import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'signup.dart';
import 'home/home.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  _LoginState createState() => _LoginState();
}

class PersonData {
  String username = '';
  String password = '';
}

class _LoginState extends State<Login> {

  PersonData person = PersonData();

  // bool _formWasEdited = false;
  bool _obscureText = true;

  String _validateName(String value) {
    // _formWasEdited = true;
    if (value.isEmpty)
      return 'Name is required.';
    final RegExp nameExp = RegExp(r'^[A-Za-z ]+$');
    if (!nameExp.hasMatch(value))
      return 'Please enter only alphabetical characters.';
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[200],
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 90, bottom: 30),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/kota.jpg"),
                fit: BoxFit.fill,
                matchTextDirection: true
              )
            ),
            child: Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                clipBehavior: Clip.hardEdge,
                child: Image.asset(
                  "assets/images/danakita.png", 
                  height: 80,
                ),
              )
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  textCapitalization: TextCapitalization.words,
                  decoration: const InputDecoration(
                    // fillColor: Colors.white,
                    filled: true,
                    border: UnderlineInputBorder(),
                    prefixIcon: Icon(Icons.person, color: Colors.white,),
                    hintText: 'What do people call you?',
                    labelText: 'Username',
                  ),
                  cursorColor: Colors.white,
                  onSaved: (String value) { person.username = value; },
                  validator: _validateName,
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  obscureText: _obscureText,
                  maxLength: 8,
                  onSaved: (String value) { person.username = value; },
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    filled: true,
                    hintText: "",
                    labelText: "Password",
                    helperText: "No more than 8 characters.",
                    prefixIcon: Icon(Icons.vpn_key, color: Colors.white,),
                    suffixIcon: GestureDetector(
                      dragStartBehavior: DragStartBehavior.down,
                      onTap: () {
                        setState(() {
                          _obscureText = !_obscureText;
                        });
                      },
                      child: Icon(
                        _obscureText ? Icons.visibility : Icons.visibility_off,
                        semanticLabel: _obscureText ? 'show password' : 'hide password',
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RaisedButton(
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  child: const Text('SIGN IN', style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)
                  ),
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (BuildContext context){
                        return Home();
                      }
                    ));
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("You do not have on account? ", style: TextStyle(color: Colors.white)),
                    InkWell(
                      child: Text("SIGN UP", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (BuildContext context){
                            return SignUp();
                          }
                        ));
                      },
                    )
                  ],
                )
              ],
            ),
          )
        ],
      )
    );
  }
}