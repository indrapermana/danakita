import 'package:flutter/material.dart';

class Cofirm extends StatelessWidget {
  const Cofirm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Confirm"),
        automaticallyImplyLeading: false,
      ),
      body: Container(
        color: Colors.blue,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text("Please cheack your email for code Verification!."),
            TextFormField(
              textCapitalization: TextCapitalization.words,
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                filled: true,
                prefixIcon: Icon(Icons.person, color: Colors.white),
                hintText: 'Code',
                labelText: 'Code',
              ),
              maxLength: 6,
            ),
            RaisedButton(
              child: Text("Verification"),
              onPressed: (){},
            )
          ],
        ),
      ),
    );
  }
}