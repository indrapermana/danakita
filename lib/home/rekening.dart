import 'package:flutter/material.dart';
import 'package:indonesia/indonesia.dart';

class Rekening extends StatefulWidget {
  final String nama;
  final String norek;
  Rekening({Key key, @required this.nama, @required this.norek}) : super(key: key);

  _RekeningState createState() => _RekeningState();
}

class _RekeningState extends State<Rekening> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.nama),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 200,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/kota.jpg"),
                fit: BoxFit.fill
              )
            ),
            child: Container(
              padding: EdgeInsets.all(10),
              color: Colors.blue.withOpacity(0.5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("SALDO", style: TextStyle(color: Colors.white.withOpacity(0.9)),),
                  SizedBox(height: 5,),
                  Text("IDR 8.400.000,00", style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500),),
                  SizedBox(height: 15,),
                  Text("HOLD AMOUNT", style: TextStyle(color: Colors.white.withOpacity(0.9)),),
                  SizedBox(height: 5,),
                  Text("IDR 0,00", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),),
                  SizedBox(height: 20,),
                  Text("Nomor Rekening", style: TextStyle(color: Colors.white.withOpacity(0.9)),),
                  SizedBox(height: 5,),
                  Text(widget.norek, style: TextStyle(color: Colors.white),),
                ],
              ),
            )
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.blue,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                InkWell(
                  child: Container(
                    width: MediaQuery.of(context).size.width*0.33,
                    padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("01 Oct 2019", style: TextStyle(color: Colors.white),),
                        Icon(Icons.arrow_drop_down, color: Colors.white,)
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width*0.3,
                  child: Text("Sampai", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,),
                ),
                InkWell(
                  child: Container(
                    width: MediaQuery.of(context).size.width*0.33,
                    padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("02 Oct 2019", style: TextStyle(color: Colors.white),),
                        Icon(Icons.arrow_drop_down, color: Colors.white,)
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                header("01 Oct 2019"),
                listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098"),
                listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098"),
                listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098"),
                header("02 Oct 2019"),
                listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098"),
                listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098"),
                listData("Arie Hidayat Lesmana", "Mandiri", "1234567890098"),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget header(String tanggal){
    return Container(
      padding: EdgeInsets.all(10),
      width: MediaQuery.of(context).size.width,
      color: Colors.grey[200],
      child: Text(tanggal),
    );
  }

  Widget listData(String nama, String bank, String norek){
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black12, width: 1, style: BorderStyle.solid))
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(nama, style: TextStyle(fontWeight: FontWeight.w500),),
                  SizedBox(height: 5,),
                  Text("$bank - $norek", style: TextStyle(color: Colors.black45),),
                ],
              ),
            ),
            Icon(Icons.arrow_forward_ios, size: 16, color: Colors.black38,)
          ],
        ),
      ),
      onTap: (){},
    );
  }
}