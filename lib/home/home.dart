import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/home/rekening.dart';
import 'package:koperasi_dana_kita/home/widget/drawer_page.dart';
import 'package:koperasi_dana_kita/home/widget/menu.dart';
import 'package:koperasi_dana_kita/menu/transfer_bank.dart';
import 'package:koperasi_dana_kita/menu/transfer_kdk.dart';

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dana Kita"),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: InkWell(
              child: Icon(Icons.assignment,),
              onTap: () {},
            ),
          )
        ],
      ),
      drawer: DrawerPage(), 
      body: ListView(
        padding: EdgeInsets.symmetric(vertical: 10),
        children: <Widget>[
          Menu(),
          SizedBox(
            height: 15,
          ),
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                    border: Border.all(color: Colors.grey[200]),
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5))
                  ),
                  child: Text(
                    "REKENING GIRO & TABUNGAN", 
                    style: TextStyle(
                      color: Colors.black54, 
                      fontWeight: FontWeight.bold, 
                      fontSize: 12
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          padding: EdgeInsets.only(left: 80, top: 50),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("assets/images/banner_danakita.jpeg"),
                              fit: BoxFit.fill,
                              matchTextDirection: true
                            ),
                            borderRadius: BorderRadius.circular(8),
                          )
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Muhammad Indra Permana", style: TextStyle(color: Colors.blue[900], fontWeight: FontWeight.w500, fontSize: 15)),
                              SizedBox(
                                height: 3,
                              ),
                              Text("987865646576", style: TextStyle(color: Colors.black45, fontSize: 12),)
                            ],
                          ),
                        ),
                        InkWell(
                          child: Icon(Icons.more_vert),
                          onTap: (){
                            _modalBottomSheet(context);
                          },
                        )
                      ],
                    ),
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return Rekening(nama: "Muhammad Indra Permana", norek: "987865646576",);
                        }
                      ));
                    },
                  ),
                )
              ],
            ),
          ),
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                    border: Border.all(color: Colors.grey[200]),
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5))
                  ),
                  child: Text(
                    "PEMBUKAAN REKENING BARU", 
                    style: TextStyle(
                      color: Colors.black54, 
                      fontWeight: FontWeight.bold, 
                      fontSize: 12
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          // padding: EdgeInsets.only(left: 60, top: 30),
                          margin: EdgeInsets.only(right: 10),
                          height: 50,
                          width: 80,
                          decoration: BoxDecoration(
                            color: Colors.grey[100],
                            borderRadius: BorderRadius.circular(8)
                          ),
                          child: Center(
                            child: Icon(Icons.add_circle, color: Colors.green[400]),
                          ),
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Pembukaan Rekening Baru", style: TextStyle(color: Colors.blue[900], fontWeight: FontWeight.w500, fontSize: 15),),
                              SizedBox(
                                height: 5,
                              ),
                              Text("Buka rekening Tabungan Rencana dan Deposito", style: TextStyle(color: Colors.black45, fontSize: 12),)
                            ],
                          ),
                        ),
                      ],
                    ),
                    onTap: (){},
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _modalBottomSheet(BuildContext context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
        return Container(
        child: new Wrap(
          children: <Widget>[
            new ListTile(
              title: new Text("AKSES CEPAT", style: TextStyle(color: Colors.black38),),
            ),
            new ListTile(
              leading: new Icon(Icons.history),
              title: new Text('Lihat History Transaksi', style: TextStyle(color: Colors.black45),),
              onTap: () => {}
            ),
            new ListTile(
              leading: new Icon(Icons.account_balance),
              title: new Text('Transfer ke Sesama Anggota', style: TextStyle(color: Colors.black45),),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(
                  builder: (context){
                    return TransferKDK();
                  }
                ));
              },
            ),
            new ListTile(
              leading: new Icon(Icons.account_balance),
              title: new Text('Transfer ke Bank Lain Dalam Negeri', style: TextStyle(color: Colors.black45),),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(
                  builder: (context){
                    return TransferBank();
                  }
                ));
              },
            ),
            new ListTile(
              title: new Text('KEMBALI', style: TextStyle(fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
              onTap: () => Navigator.pop(context),
            ),
          ],
        ),
      );
    });
  }
}