import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/drawel/profile.dart';
import 'package:koperasi_dana_kita/login.dart';

class DrawerPage extends StatelessWidget {
  const DrawerPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/kota.jpg"),
                fit: BoxFit.fill
              ),
            ),
            padding: EdgeInsets.all(0),
            child: Container(
              color: Colors.black.withOpacity(0.35),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white, width: 2, style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: CircleAvatar(
                      backgroundImage: AssetImage("assets/images/profile.jpg"),
                      radius: 40,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "MUHAMMAD INDRA PERMANA", 
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                    )
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
          listMenu("Profile Saya", Icons.person, (){
            Navigator.push(context, MaterialPageRoute(
              builder: (context){
                return Profile();
              }
            ));
          }),
          listMenu("Peraturan", Icons.settings,() {}),
          listMenu("Status Transaksi", Icons.bookmark_border,() {}),
          listMenu("Suku Bunga", Icons.equalizer,() {}),
          listMenu("Bantuan", Icons.help_outline, () {}),
          listMenu("Tentang Dana Kita Online", Icons.error_outline, () {}),
          listMenu("Hubungi Kami", Icons.call, () {}),
          InkWell(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.black12, width: 1, style: BorderStyle.solid))
              ),
              child: Row(
                children: <Widget>[
                  Icon(Icons.exit_to_app),
                  SizedBox(width: 10,),
                  Expanded(
                    child: Text("Logout", style: TextStyle(fontWeight: FontWeight.w500),),
                  ),
                ],
              ),
            ),
            onTap: () {
              Navigator.pushReplacement(
                context, 
                MaterialPageRoute(
                  builder: (BuildContext context){
                    return Login();
                  }
                )
              );
            },
          ),
        ],
      ),
    );
  }

  Widget listMenu(String _titile, IconData icon, _onTap){
    return InkWell(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black12, width: 1, style: BorderStyle.solid))
        ),
        child: Row(
          children: <Widget>[
            Icon(icon),
            SizedBox(width: 10,),
            Expanded(
              child: Text(_titile, style: TextStyle(fontWeight: FontWeight.w500),),
            ),
            Icon(Icons.arrow_forward_ios, color: Colors.black38,)
          ],
        ),
      ),
      onTap: _onTap,
    );
  }
}