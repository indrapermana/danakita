import 'package:flutter/material.dart';
import 'package:koperasi_dana_kita/home/home.dart';
import 'package:koperasi_dana_kita/menu/create_user.dart';
import 'package:koperasi_dana_kita/menu/pinjam_uang.dart';
import 'package:koperasi_dana_kita/menu/setor_tunai.dart';
import 'package:koperasi_dana_kita/menu/tarik_tunai.dart';
import 'package:koperasi_dana_kita/menu/transaksi_tertunda.dart';
import 'package:koperasi_dana_kita/menu/bayar.dart';
import 'package:koperasi_dana_kita/menu/beli.dart';
import 'package:koperasi_dana_kita/menu/e_money.dart';
import 'package:koperasi_dana_kita/menu/favoritku.dart';
import 'package:koperasi_dana_kita/menu/transfer.dart';
import 'package:koperasi_dana_kita/pin.dart';

class Menu extends StatelessWidget {
  const Menu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, top: 15.0, right: 15.0, bottom: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              btnIcon(Colors.green, "Transfer", context, "icon", icon: Icons.input),
              btnIcon(Colors.blue, "Bayar", context, "icon", icon: Icons.description),
              btnIcon(Colors.yellow[600], "Beli", context, "icon", icon: Icons.shopping_cart),
              btnIcon(Colors.orange, "E-Money", context, "icon", icon: Icons.phonelink_ring),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 14, top: 15, right: 14, bottom: 5),
                child: btnIcon(Colors.red[600], "Transaksi Tertunda", context, "icon", icon: Icons.alarm),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 14, top: 15, right: 14, bottom: 5),
                child: btnIcon(Colors.pink[300], "Favoritku", context, "icon", icon: Icons.star),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 14, top: 15, right: 14, bottom: 5),
                child: btnIcon(Colors.deepPurple, "PIN", context, "icon", icon: Icons.vpn_key),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 14, top: 15, right: 0, bottom: 5),
                child: btnIcon(Colors.blueGrey, "Buat Akun", context, "icon", icon: Icons.person_add),
              ),
              
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 14, top: 10, right: 14, bottom: 5),
                child: btnIcon(Colors.red[600], "Setor", context, "icon", icon: Icons.account_balance_wallet,),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 14, top: 10, right: 8, bottom: 5),
                child: btnIcon(Colors.red[600], "Tarik", context, "gambar", gambar: "tarik_uang.png",),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 14, top: 10, right: 14, bottom: 5),
                child: btnIcon(Colors.red[600], "Pinjam", context, "gambar", gambar: "pinjam_uang.png",),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget btnIcon(Color bgColor, String label, BuildContext context, String type, {IconData icon, String gambar}) {
    return InkWell(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          // Container(
          //   padding: EdgeInsets.all(10),
          //   decoration: BoxDecoration(
          //     color: bgColor,
          //     borderRadius: BorderRadius.circular(50)
          //   ),
          //   child: (type=="icon")? Icon(icon, size: 35, color: Colors.white,) : Image.asset("assets/images/$gambar", width: 50,),
          // ),
          icons(type, bgColor, icon: icon, gambar: gambar),
          
          // Text("Transfer", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),)
        ],
      ),
      onTap: () => onPress(label, context),
    );
  }

  Widget icons(String type, Color bgColor, {IconData icon, String gambar}){
    if(type=="icon"){
      return Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(50)
        ),
        child: Icon(icon, size: 35, color: Colors.white,),
      );
    } 
    return Image.asset("assets/images/$gambar", width: 60,);
  }

  void onPress(String label, BuildContext context){
    if (label == "Transfer") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return Transfer();
          }
        )
      );
    } else if (label == "Bayar") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return Bayar();
          }
        )
      );
    } else if (label == "Beli") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return Beli();
          }
        )
      );
    } else if (label == "E-Money") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return EMoney();
          }
        )
      );
    } else if (label == "Transaksi Tertunda") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return TransaksiTertunda();
          }
        )
      );
    } else if (label == "PIN") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return PinPage();
          }
        )
      );
    } else if (label == "Buat Akun") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return CreateUserPage();
          }
        )
      );
    } else if (label == "Setor") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return SetorTunai();
          }
        )
      );
    } else if (label == "Tarik") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return TarikTunai();
          }
        )
      );
    } else if (label == "Favoritku") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return Favoritku();
          }
        )
      );
    } else if (label == "Pinjam") {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return PinjamUangPage();
          }
        )
      );
    } else {
      Navigator.push(
        context, 
        MaterialPageRoute(
          builder: (BuildContext context){
            return Home();
          }
        )
      );
    }
  }
}